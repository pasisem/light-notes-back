(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn{\n    width: 100%;\n    margin-top: 1.6rem;\n}\n.row{\n    margin-bottom: 5px;\n}\n.sidenav-trigger{\n    font-size: 30px\n}\n.card{\n    transition: 0.3s;\n}\n.center-text{\n    text-align: center;\n}\n.progress{\n    margin: 0;\n}"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"teal lighten-2\">\n  <div class=\"container nav-wrapper\">\n    <a class=\"brand-logo\">{{title}}</a>\n    <a href=\"#\" data-target=\"sidenav-menu\" class=\"sidenav-trigger\">&#9776;</a>\n    <ul id=\"nav-mobile\" class=\"right hide-on-med-and-down\">\n      <li><a (click)=\"getNotes(false)\">Latest</a></li>\n      <li><a (click)=\"getNotes(true)\">Oldest</a></li>\n    </ul>\n  </div>\n</nav>\n\n<ul id=\"sidenav-menu\" class=\"sidenav\">\n    <li class=\"teal lighten-1 section white-text\">\n      <h5 class=\"center-text\">{{title}}</h5>\n    </li>\n    <li><a class=\"waves-effect\" (click)=\"getNotes(false)\">Latest first</a></li>\n    <li><a class=\"waves-effect\" (click)=\"getNotes(true)\">Oldest first</a></li>\n</ul>\n<div class=\"progress\" *ngIf=\"loading\">\n    <div class=\"indeterminate\"></div>\n</div>\n\n\n<div class=\"container\">\n    <div class=\"section\">\n        <div class=\"card\">\n          <div class=\"card-content\">\n            <h5 class=\"responsive-text\">New Note</h5>\n            <div class=\"row\">\n              <div class=\"input-field col s12\">\n                <textarea id=\"note-text\" class=\"materialize-textarea\" [(ngModel)]=\"newNote.text\"></textarea>\n                <label for=\"note-text\" >Note</label>\n              </div>\n            </div>\n      \n            <div class=\"row\">\n                <div class=\"input-field col s12 m6\">\n                  <input id=\"name\" type=\"text\" class=\"\" [(ngModel)]=\"newNote.name\">\n                  <label for=\"name\">Name</label>\n                </div>\n                <div class=\"col s6 m3\">\n                  <a class=\"btn\" (click)=\"emptyForm()\" *ngIf=\"newNote.name || newNote.text\">Clear</a>\n                </div>\n                <div class=\"col s6 m3\">\n                  <a class=\"btn\" (click)=\"addNote()\" *ngIf=\"newNote.name && newNote.text\">Submit</a>\n                </div>\n              </div>\n          </div>\n        </div>\n      </div>\n      \n      <h5 class=\"responsive-text\" *ngIf=\"notes.length > 0\">Notes</h5>\n      <p class=\"responsive-text grey-text text-lighten-1 center-text\" *ngIf=\"notes.length <= 0\">No notes yet... Go ahead and add one!</p>\n      <div class=\"section\" *ngFor=\"let note of notes\">\n        <div class=\"card lighten-2 white-text\" [ngClass]=\"{'cyan': editingCheck(note), 'teal': !editingCheck(note)}\">\n          <div class=\"card-content\">\n      \n            <div class=\"row\">\n              <div class=\"input-field col s12\" *ngIf=\"editingCheck(note)\">\n                <textarea id=\"note-text\" class=\"materialize-textarea white-text\" [(ngModel)]=\"selectedNote.text\"></textarea>\n                <label for=\"note-text\" class=\"white-text active\">Note</label>\n              </div>\n              <div class=\"col s12\" *ngIf=\"!editingCheck(note)\">\n                <p class=\"responsive-text white-text\">\n                  {{note.text}}\n                </p>\n              </div>\n            </div>\n      \n            <div class=\"row\">\n                <div class=\"input-field col s12 m6\" *ngIf=\"editingCheck(note)\">\n                  <input id=\"name\" type=\"text\" class=\"white-text\" [(ngModel)]=\"selectedNote.name\">\n                  <label for=\"name\" class=\"white-text active\">Name</label>\n                </div>\n                <div class=\"input-field col s12 m6\" *ngIf=\"!editingCheck(note)\">\n                  <h5 class=\"responsive-text white-text\">{{note.name}}</h5>\n                </div>\n                <div class=\"normal-buttons button-container\" *ngIf=\"!editingCheck(note)\">\n                  <div class=\"col s6 m3\">\n                      <a class=\"btn\" (click)=\"selectNoteEdit(note)\">Edit</a>\n                  </div>\n                  <div class=\"col s6 m3\">\n                      <a class=\"btn modal-trigger\" href=\"#delete-modal\" (click)=\"selectNote(note)\">Delete</a>\n                  </div>\n                </div>\n                <div class=\"edit-mode-buttons button-container\" *ngIf=\"editingCheck(note)\">\n                    <div class=\"col s6 m3\">\n                        <a class=\"btn\" (click)=\"unselectNote()\">Cancel</a>\n                    </div>\n                    <div class=\"col s6 m3\">\n                        <a class=\"btn\" (click)=\"editNote(selectedNote)\">Save</a>\n                    </div>\n                  </div>\n              </div>\n              <div class=\"col s12 row white-text\">\n                  <span class=\"col s12 m6 responsive-text\">Posted: {{ dateToLocal(note.created) }}</span>\n                  <span class=\"col s12 m6 responsive-text\" *ngIf=\"note.updated\">Edited: {{ dateToLocal(note.updated) }}</span>\n              </div>\n          </div>\n        </div>\n      </div>\n      \n      \n      <div id=\"delete-modal\" class=\"modal\">\n        <div class=\"modal-content\" *ngIf=\"selectedNote\">\n          <h4>Deleting a note</h4>\n          <p>Are you sure you want to delete this note by \"{{selectedNote.name}}\"?</p>\n          <blockquote>\n              {{selectedNote.text}}\n          </blockquote>\n        </div>\n        <div class=\"modal-footer\">\n          <button type=\"button\" class=\"modal-close btn-flat\" (click)=\"deleteNote(selectedNote._id)\" *ngIf=\"selectedNote\">Yes</button>\n          <button type=\"button\" class=\"modal-close btn-flat\" (click)=\"unselectNote()\">No</button>\n        </div>\n      </div>\n      \n</div>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_data_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./services/data.service */ "./src/app/services/data.service.ts");
/* harmony import */ var materialize_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! materialize-css */ "./node_modules/materialize-css/dist/js/materialize.js");
/* harmony import */ var materialize_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(materialize_css__WEBPACK_IMPORTED_MODULE_2__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AppComponent = /** @class */ (function () {
    function AppComponent(data) {
        this.data = data;
        this.title = "Notes";
    }
    AppComponent.prototype.ngOnInit = function () {
        this.notes = [];
        this.editing = false;
        this.unselectNote();
        this.emptyForm();
        this.reverseOrder = false;
        this.getNotes(this.reverseOrder);
        // materialize modal popup
        document.addEventListener('DOMContentLoaded', function () {
            var elems = document.querySelectorAll('.modal');
            var instances = materialize_css__WEBPACK_IMPORTED_MODULE_2__["Modal"].init(elems, {});
        });
        // materialize sidenav
        document.addEventListener('DOMContentLoaded', function () {
            var elems = document.querySelectorAll('.sidenav');
            var instances = materialize_css__WEBPACK_IMPORTED_MODULE_2__["Sidenav"].init(elems, {});
        });
    };
    AppComponent.prototype.selectNote = function (note) {
        this.selectedNote = {
            _id: note._id,
            text: note.text,
            name: note.name,
            created: note.created,
            updated: note.updated
        };
    };
    AppComponent.prototype.selectNoteEdit = function (note) {
        this.editing = true;
        this.selectNote(note);
    };
    AppComponent.prototype.unselectNote = function () {
        this.editing = false;
        this.selectedNote = null;
    };
    AppComponent.prototype.emptyForm = function () {
        this.newNote = {
            _id: null,
            text: null,
            name: null,
            created: null,
            updated: null
        };
    };
    // markup helpers
    AppComponent.prototype.editingCheck = function (note) {
        if (!this.editing || !this.selectedNote || note._id != this.selectedNote._id) {
            return false;
        }
        else {
            return true;
        }
    };
    AppComponent.prototype.dateToLocal = function (dateString) {
        var newDate = new Date(parseInt(dateString));
        return newDate.toLocaleTimeString() + " - " + newDate.toLocaleDateString();
    };
    // data
    AppComponent.prototype.getNotes = function (reverse) {
        var _this = this;
        this.loading = true;
        this.data.getNotes(reverse).subscribe(function (data) {
            _this.loading = false;
            _this.notes = data;
        });
    };
    AppComponent.prototype.addNote = function () {
        var _this = this;
        this.loading = true;
        this.data.addNote(this.newNote).subscribe(function (note) {
            _this.getNotes(_this.reverseOrder);
            _this.emptyForm();
        });
    };
    AppComponent.prototype.editNote = function (note) {
        var _this = this;
        this.loading = true;
        this.data.updateNote(note).subscribe(function (note) {
            _this.unselectNote();
            _this.getNotes(_this.reverseOrder);
        });
    };
    AppComponent.prototype.deleteNote = function (id) {
        var _this = this;
        this.loading = true;
        this.data.deleteNote(id).subscribe(function (note) {
            _this.getNotes(_this.reverseOrder);
        });
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [_services_data_service__WEBPACK_IMPORTED_MODULE_1__["DataService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/services/data.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/data.service.ts ***!
  \******************************************/
/*! exports provided: DataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataService", function() { return DataService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DataService = /** @class */ (function () {
    function DataService(http) {
        this.http = http;
        // this.url = "http://localhost:3000/api";
        this.url = "/api";
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                'Content-Type': 'application/json'
            })
        };
    }
    DataService.prototype.getNotes = function (reverse) {
        return this.http.get(this.url + '/notes').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (res) {
            if (reverse) {
                return res.reverse();
            }
            else {
                return res;
            }
        }));
    };
    DataService.prototype.getNote = function (id) {
        return this.http.get(this.url + '/note/' + id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (res) {
            return res;
        }));
    };
    DataService.prototype.addNote = function (newNote) {
        return this.http.post(this.url + '/note', JSON.stringify(newNote), this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (res) {
            return res;
        }));
    };
    DataService.prototype.updateNote = function (updatedNote) {
        return this.http.put(this.url + '/note/' + updatedNote._id, JSON.stringify(updatedNote), this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (res) {
            return res;
        }));
    };
    DataService.prototype.deleteNote = function (id) {
        console.log(id);
        return this.http.delete(this.url + '/note/' + id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (res) {
            return res;
        }));
    };
    DataService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], DataService);
    return DataService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/psem/Workspace/Angular/light-notes/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map