var express = require('express');
var router = express.Router();
var mongojs = require('mongojs');
var db = mongojs('< DB URL >', ['notes']);

// get all notes
router.get('/notes', function(req, res, next){
    db.notes.find().sort({created: -1}, function(err, notes){
        if(err){
            res.send(err);
        }
        res.json(notes);
    });
});

// get single note
router.get('/note/:id', function(req, res, next){
    db.notes.findOne({_id: mongojs.ObjectId(req.params.id)}, function(err, note){
        if(err){
            res.send(err);
        }
        res.json(note);
    });
});

// save note
router.post('/note', function(req, res, next){
    var note = req.body;

    if(!note.text || !note.name || note.text.length > 100000 || note.name > 1000000 || note.text.length < 1 || note.name < 1){
        res.status(400);
        res.json({
            "error": "bad note data",
            "data": note
        });
    } else {
        sendData();
    }
    function sendData(){
        note._id = mongojs.ObjectId();
        note.created = Date.now().toString();
        
        db.notes.save(note, function(err, note){
            if(err){
                res.send(err);
            }
            res.json(note);
        })
    }
});

// update note
router.put('/note/:id', function(req, res, next){
    var note = req.body;
    var updatedNote = {};
    
    if(note.text){
        updatedNote.text = note.text;
    }
    if(note.name){
        updatedNote.name = note.name;
    }
    if(note.created){
        updatedNote.created = note.created;
    }
    updatedNote.updated = Date.now().toString();
    
    if(!updatedNote.text || !updatedNote.name || !updatedNote.created || note.text.length > 100000 || note.name > 1000000 || note.text.length < 1 || note.name < 1){
        res.status(400);
        res.json({
            "error": "bad updated note data",
            "data": note
        });
    } else {
        sendData();
    }

    function sendData(){
        db.notes.update({_id: mongojs.ObjectId(req.params.id)}, updatedNote, {}, function(err, note){
            if(err){
                res.send(err);
            }
            res.json(note);
        });
    }
});

// delete note by id
router.delete('/note/:id', function(req, res, next){
    db.notes.remove({_id: mongojs.ObjectId(req.params.id)}, function(err, note){
        if(err){
            res.send(err);
        }
        res.json(note);
    });
});

module.exports = router;